/***************************************************************************
*   Copyright (C) 2004-2012 by Thomas Fischer                             *
*   fischer@unix-ag.uni-kl.de                                             *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, write to the                         *
*   Free Software Foundation, Inc.,                                       *
*   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
***************************************************************************/

#include <QRegExp>
#include <QStringList>

#include "macro.h"

/**
 * Private class to store internal variables that should not be visible
 * in the interface as defined in the header file.
 */
class Macro::MacroPrivate
{
public:
    QString key;
    Value value;
};

Macro::Macro(const QString &key, const Value &value)
        : Element(), d(new Macro::MacroPrivate)
{
    d->key = key;
    d->value = value;
}

Macro::Macro(const Macro &other)
        : Element(), d(new Macro::MacroPrivate)
{
    d->key = other.d->key;
    d->value = other.d->value;
}

Macro::~Macro()
{
    delete d;
}

Macro &Macro::operator= (const Macro &other)
{
    if (this != &other) {
        d->key = other.key();
        d->value = other.value();
    }
    return *this;
}

void Macro::setKey(const QString &key)
{
    d->key = key;
}

QString Macro::key() const
{
    return d->key;
}

Value &Macro::value()
{
    return d->value;
}

const Value &Macro::value() const
{
    return d->value;
}

void Macro::setValue(const Value &value)
{
    d->value = value;
}

